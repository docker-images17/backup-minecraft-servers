FROM alpine:3.16

ENV TZ="Europe/Lisbon"

RUN apk add --no-cache tzdata

COPY backup.sh /

ENTRYPOINT [ "/backup.sh" ]