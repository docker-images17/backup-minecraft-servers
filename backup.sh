#!/usr/bin/env sh

set -euo pipefail

MINECRAFT_DIR="/minecraft"
BACKUP_DIR="/backup"
NOW="$(date +'%b-%d-%y_%H-%M')"

FILE=""$BACKUP_DIR"/"$NOW"_minecraft-"$STACK".tar.gz"

echo "Creating file "$FILE""

tar -czf "$FILE" "$MINECRAFT_DIR" 2>&1

echo "Changing file permissions:"
chown 1000:1000 "$FILE"
ls -lah "$FILE"

echo
echo "Fiding and prunning old backups"
find "$BACKUP_DIR" -type f -mtime +7 -delete

echo "Finished backing up the minecraft server: "$STACK""
