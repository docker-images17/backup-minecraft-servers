#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"

INPUT="script.json"
TAG="$(jq --raw-output .tag "$INPUT")"

IMAGE="rafaelmoreira178/minecraft:"$TAG""

docker buildx build . \
    --push \
    --platform="linux/arm64,linux/amd64" \
    --tag="$IMAGE"
